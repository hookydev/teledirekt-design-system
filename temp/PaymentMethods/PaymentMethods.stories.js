
import React from 'react';
import { storiesOf } from '@storybook/react';

import PaymentMethods from './PaymentMethods';

storiesOf('PaymentMethods', module)
    .add('default', () => <PaymentMethods/>)

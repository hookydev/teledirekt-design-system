import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";

const Container = styled.div`
    font-family: ${props => props.theme.fontFamily.secondary};
    font-weight: bold;
`;

const Heading = styled.legend`
    font-size: 1.25rem;
    margin-bottom: 1.5625rem;
`;

const PaymentList = styled.ul`
    padding: 0;
    margin: 0;
    font-size: 1rem;
    display: flex;
    flex-wrap: wrap;
    justify-content: space-between;
    list-style-type: none;
    padding-left: 1.25rem;
`;

const PaymentItem = styled.li`
    position: relative;
    white-space: nowrap;
    margin-bottom: 0.75rem;
    padding-left: 15px;

    &::before {
        content: "";
        position: absolute;
        left: 0;
        top: 50%;
        transform: translateY(-50%);
        background-color: currentColor;
        width: 6px;
        height: 6px;
        border-radius: 100%;
    }
`;

const DiscountLabel = styled.span`
    padding: 0.17em 0.25em;
    margin-left: 5px;
    white-space: nowrap;
    font-size: 0.75rem;
    border-radius: 2em;
    color: ${props => props.theme.colors.forms.label.color};
    background-color: ${props => props.theme.colors.forms.label.bgColor};
`;

const PaymentMethods = ({ discountCard, discountCredit }) => (
    <Container>
        <Heading>Возможные способы оплаты</Heading>
        <PaymentList>
            <PaymentItem>Наличными</PaymentItem>
            <PaymentItem>
                Картой <DiscountLabel>Скидка -{discountCard}%</DiscountLabel>
            </PaymentItem>
            <PaymentItem>
                В рассрочку{" "}
                <DiscountLabel>Скидка -{discountCredit}%</DiscountLabel>
            </PaymentItem>
        </PaymentList>
    </Container>
);

PaymentMethods.defaultProps = {
    discountCard: "10",
    discountCredit: "10"
};

PaymentMethods.propTypes = {
    discountCard: PropTypes.oneOf([PropTypes.string, PropTypes.number]),
    discountCredit: PropTypes.oneOf([PropTypes.string, PropTypes.number])
};

export default PaymentMethods;

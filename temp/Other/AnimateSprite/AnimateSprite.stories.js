import React from 'react';
import { storiesOf } from '@storybook/react';

import AnimateSprite from "./AnimateSprite";

const spriteSettings = {
    width: 384,
    height: 399,
    frames: 3,
    duration: 3000
}

storiesOf('Other/AnimateSprite', module)
    .add('default', () => (
        <AnimateSprite {...spriteSettings}>
             <img src={require('./images/animation.png')} alt=""/>
      </AnimateSprite>
    ))

import React from 'react';
import { storiesOf } from '@storybook/react';

import IconSimple from './Icon';

storiesOf('Other/IconSimple', module)
    .add('default', () => <IconSimple name='block' size='48' color={'#4e7bf9'}/>)


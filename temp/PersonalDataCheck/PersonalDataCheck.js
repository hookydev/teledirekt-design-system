import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";

import Checkbox from "./../Atoms/Checkbox";


const Container = styled.div`
    font-family: ${props => props.theme.fonts.secondary};
    font-weight: 300;
    color: ${props => props.theme.colors.neutral.gray[100]};
    display: flex;
    align-items: flex-start;
    background: ${props => props.theme.colors.neutral.gray[0]};
    border: 2px solid
        ${props => props.theme.colors.neutral.gray[40]};
`;

const Text = styled.p`
    font-size: 12px;
    margin-bottom: 20px;
`;

const Link = styled.a`
    color: currentColor;
    text-decoration: underline;
`;

const PersonalDataCheck = () => (
    <Container>
        {/* <Checkbox variant="primary" /> */}
        <Text>Нажимая кнопку «Отправить заказ», я даю согласие на</Text>

    </Container>
);

// PersonalDataCheck.defaultProps = {
//     discountCard: "10",
//     discountCredit: "10"
// };

// PersonalDataCheck.propTypes = {
//     discountCard: PropTypes.oneOf([PropTypes.string, PropTypes.number]),
//     discountCredit: PropTypes.oneOf([PropTypes.string, PropTypes.number])
// };

export default PersonalDataCheck;

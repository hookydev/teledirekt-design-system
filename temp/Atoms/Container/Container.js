import styled from "styled-components";
import { Box } from "./../Grid/Grid.js";

const Container = styled(Box)`
    ${p =>
        !p.fixed &&
        p.theme.mediaQueries.forEach(
            (mediaQuery, i) =>
                `${mediaQuery} { max-width: ${p.theme.maxContainerWidths[i]}}`
        )}

    max-width: ${p => p.fixed && p.theme.fixedContainerWidth};
`;

Container.defaultProps = {
    mx: "auto"
};

Container.displayName = "Container";

export default Container;

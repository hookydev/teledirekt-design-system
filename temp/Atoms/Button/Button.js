import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";

const ButtonStyled = styled.button`
    display: inline-flex;
    position: relative;
    appearance: none;
    user-select: none;
    align-items: center;
    justify-content: center;
    text-decoration: none;
    text-align: center;
    padding: 1em 2em;
    cursor: pointer;
    border: none;
    outline: none;
    width: ${props => props.fullWidth && "100%"};
    font-size: ${props => props.theme.buttonSizes[props.size] || props.size};
    min-width: ${props =>
        props.withoutMinWidth ? null : Button.defaultProps.minWidth};

    ${props => props.theme.variants.button.base};
    ${props => props.theme.variants.button[props.variant] || props.variant};
`;

const Button = ({ children, size, variant, minWidth, ...props }) => (
    <ButtonStyled variant={variant} size={size} minWidth={minWidth} {...props}>
        {children}
    </ButtonStyled>
);

Button.propTypes = {
    children: PropTypes.node.isRequired,
    variant: PropTypes.string,
    size: PropTypes.string,
    minWidth: PropTypes.string,
    fullWidth: PropTypes.bool
};

Button.defaultProps = {
    children: "Button",
    minWidth: "11em",
    variant: "prime",
    size: "default"
};

export default Button;

import React from "react";
import { storiesOf } from "@storybook/react";
import { css } from "styled-components";
import { darken } from "polished";

import Button from "./Button";

import Icon from "../Icon";

const activated = css`
    color: ${props => props.theme.colors.neutral.white};
    background: ${props => props.theme.colors.semantic.error.base};
    border-color: ${props => props.theme.colors.semantic.error.base};

    &:hover,
    &:focus {
        background: ${props =>
            darken(0.3, props.theme.colors.semantic.error.base)};
    }
`;

storiesOf("Atoms/Button", module)
    .add("default", () => <Button>Заказать</Button>)
    .add("with variant", () => <Button variant="second">Заказать 2 шт.</Button>)
    .add("with size", () => (
        <Button variant="prime" size="big">
            Заказать 2 шт.
        </Button>
    ))
    .add("full width", () => (
        <Button variant="prime" fullWidth>
            Заказать 2 шт.
        </Button>
    ))
    .add("without minWidth", () => <Button>Заказать</Button>)
    .add("width icon as child left", () => (
        <Button>
            <Icon name="check" style={{ marginRight: "5px" }} />
            Заказать
        </Button>
    ))
    .add("width icon as child right", () => (
        <Button>
            <Icon name="check" style={{ marginRight: "5px" }} />
            Заказать
            <Icon name="menu" size={"20"} style={{ marginLeft: "5px" }} />
        </Button>
    ))

    .add("button with ondemand variant", () => (
        <Button variant={activated}>
            <Icon name="check" style={{ marginRight: "5px" }} />
            Заказать
            <Icon name="menu" size={"20"} style={{ marginLeft: "5px" }} />
        </Button>
    ))
    .add("all sizes", () => (
        <div>
            <Button size="small" style={{ marginRight: "10px" }}>
                Заказать
            </Button>
            <Button size="default" style={{ marginRight: "10px" }}>
                Заказать
            </Button>
            <Button size="big" style={{ marginRight: "10px" }}>
                Заказать
            </Button>
        </div>
    ));

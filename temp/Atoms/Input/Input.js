import React from "react";
import PropTypes from "prop-types";

import {
    InputWrapper,
    InputStyled,
    Label,
    TooltipError
} from "./styles";

const Input = ({
    type,
    value,
    state,
    label,
    variant,
    required,
    fontSize,
    color,
    bgColor,
    borderColor,
    labelColor,
    tooltipText,
    onInputChange
}) => {
    return (
        <InputWrapper>
            <InputStyled
                type={type}
                value={value}
                state={state}
                variant={variant}
                disabled={state === "INPUT_DISABLED"}
                onChange={onInputChange}
                fontSize={fontSize}
                color={color}
                bgColor={bgColor}
                borderColor={borderColor}
            />
            <Label fontSize={fontSize} labelColor={labelColor} variant={variant}>
                {label}
                {required && <span>*</span>}
            </Label>
            {state === "INPUT_ERROR" && (
                <TooltipError>{tooltipText}</TooltipError>
            )}
        </InputWrapper>
    );
};

Input.displayName = "Input";

Input.propTypes = {
    type: PropTypes.string,
    required: PropTypes.bool
};

Input.defaultProps = {
    type: "text",
    label: "Текст",
    value: "",
    variant: 'prime',
    tooltipText: "Это поле необходимо заполнить"
};

export default Input;

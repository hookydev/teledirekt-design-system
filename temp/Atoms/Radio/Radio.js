import React from "react";
import styled from "styled-components";
import PropTypes from "prop-types";

const IconCheck = styled.span`
    width: 1em;
    height: 1em;
    font-size: 0.58em;
    border-radius: 100%;
    background-color: currentColor;
`;

const Button = styled(({ tag = 'div', children, ...props }) => e(tag, props, children)) `
  display: inline-flex;
 `

const Mark = styled.span`
    display: inline-flex;
    flex-shrink: 0;
    align-items: center;
    justify-content: center;
    position: relative;
    user-select: none;
    cursor: ${props => (props.disabled ? "not-allowed" : "pointer")};
    box-sizing: content-box;
    width: 1em;
    height: 1em;
    font-size: ${props => props.size}px;
    ${props => props.theme.variants.radio[props.variant]};

    color: ${props => props.disabled && props.theme.colors.neutral.bg};
    background: ${props =>
        props.disabled && props.theme.colors.neutral.gray[78]};
    border-color: ${props =>
        props.disabled && props.theme.colors.neutral.gray[78]};
`;

const Input = styled.input`
    position: absolute;
    display: none;
    z-index: -2;

    ~ ${Mark} * {
        visibility: hidden;
        opacity: 0;
    }

    &:checked ~ ${Mark} {
        border-color: currentColor;
    }

    &:checked ~ ${Mark} * {
        visibility: visible;
        opacity: 1;
    }
`;

const Radio = ({
    checked,
    disabled,
    variant,
    size,
    name,
    value,
    required,
    autofocus,
    ...props
}) => (
    <React.Fragment>
        <Input
            type="radio"
            checked={checked}
            name={name}
            value={value}
            disabled={disabled}
            required={required}
            autofocus={autofocus}
        />
        <Mark
            size={size}
            variant={variant}
            checked={checked}
            disabled={disabled}
            role="radio"
            {...props}
        >
            <IconCheck />
        </Mark>
    </React.Fragment>
);

Radio.defaultProps = {
    size: 19,
    variant: "prime",
    fontSize: "0.875rem"
};

Radio.propTypes = {
    checked: PropTypes.bool,
    disabled: PropTypes.bool,
    required: PropTypes.bool,
    autofocus: PropTypes.bool,
    variant: PropTypes.string,
    size: PropTypes.number,
    name: PropTypes.string,
    value: PropTypes.string
};

export default Radio;

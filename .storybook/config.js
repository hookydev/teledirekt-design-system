import React from 'react'
import { injectGlobal, ThemeProvider } from 'styled-components'
import { configure, addDecorator } from '@storybook/react'
import { setDefaults, withInfo } from '@storybook/addon-info'
// import { setOptions } from "@storybook/addon-options";
import { withKnobs, text, boolean, number } from '@storybook/addon-knobs'

import Theme from './../src/theme'

// options-info
// setOptions({
//     showAddonPanel: false
// });

// addon-info
setDefaults({
    inline: true,
    header: false,
    styles: {
        header: {
            h1: {
                fontSize: '25px',
            },
            h2: {
                fontSize: '16px',
            },
        },
        propTableHead: {
            margin: '20px 0',
        },
    },
})

// Configure to find all files with stories inside each component
const req = require.context('../src/components', true, /\.stories\.js$/)

function loadStories() {
    req.keys().forEach(filename => req(filename))
}

// Info addon decoratror
addDecorator((story, context) => withInfo('')(story)(context))

// Theme decorator
addDecorator(story => <Theme>{story()}</Theme>)

// Styling decorator
addDecorator(story => <div style={{ margin: '30px' }}>{story()}</div>)

configure(loadStories, module)

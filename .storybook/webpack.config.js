module.exports = {
  module: {
    rules: [
      {
        test: /\.css$/,
        use: [
          require.resolve('style-loader'),
          {
            loader: require.resolve('css-loader'),
            options: {
              importLoaders: 1,
            },
          },
          {
            loader: require.resolve('postcss-loader'),
            options: {
              ident: 'postcss', // https://webpack.js.org/guides/migrating/#complex-options
              plugins: () => [
                require('postcss-flexbugs-fixes'), // eslint-disable-line
                autoprefixer({
                  browsers: ['>1%', 'last 4 versions', 'Firefox ESR', 'not ie < 9'],
                  flexbox: 'no-2009',
                }),
              ],
            },
          },
        ],
      },
      {
        test: /\.json$/,
        loader: require.resolve('json-loader'),
      },
      {
        test: /\.(ico|jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2)(\?.*)?$/,
        loader: require.resolve('file-loader'),
        query: {
          name: 'static/media/[name].[hash:8].[ext]',
        },
        exclude: /\.inline.svg$/,
      },
      {
        test: /\.inline.svg$/,
        use: {
          loader: '@svgr/webpack',
          options: {
            icon: true,
            expandProps: true,
            filenameCase: 'camel',
            svgoConfig: {
              plugins: [
                { removeAttrs: { attrs: 'class' } },
                { mergePaths: true },
                { convertShapeToPath: true },
                { convertPathData: true }
              ]
            }
          }
        },
      },
      {
        test: /\.(mp4|webm|wav|mp3|m4a|aac|oga)(\?.*)?$/,
        loader: require.resolve('url-loader'),
        query: {
          limit: 10000,
          name: 'static/media/[name].[hash:8].[ext]',
        },
      },
    ],
  }
};


// module.exports = (baseConfig, env, defaultConfig) => {
//   // Extend defaultConfig as you need.

//   // For example, add typescript loader:
//   defaultConfig.module.rules.push(
//     {
//       test: /\.svg$/,
//       use: ['@svgr/webpack', 'file-loader'],
//     }
//   );

//   return defaultConfig;
// };

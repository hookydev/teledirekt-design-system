// shorthand for `@media screen and (min-width: ${n}), where ${n} is breakpoint alias (for simple queries)
export const media = {
    xs: css => p => p.theme.media.xs(css),
    sm: css => p => p.theme.media.sm(css),
    md: css => p => p.theme.media.md(css),
    lg: css => p => p.theme.media.lg(css),
    xl: css => p => p.theme.media.xl(css),
    xxl: css => p => p.theme.media.xxl(css),
}

// shorthand for getting breakpoint value with alias from theme (for complex queries)
export const bp = val => p => (p.theme.breakpoints && p.theme.breakpoints[val]) || null



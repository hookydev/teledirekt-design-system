import React from 'react'
import { storiesOf } from '@storybook/react'

import InfoBlock from './InfoBlock'

storiesOf('Components/App/InfoBlock', module)
    .add('deafult', () => <InfoBlock />)
    .add('error', () => <InfoBlock error />)

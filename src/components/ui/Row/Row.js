import React from 'react'
import { ThemeConsumer } from 'styled-components'
import Flex from '../Flex'

const Row = props => (
    <ThemeConsumer>
        {theme => (
            <Flex
                mx={props.mx || (theme && theme.gutters.map(n => -n)) || ['8px', '8px', '16px']}
                {...props}
            />
        )}
    </ThemeConsumer>
)

Row.defaultProps = {
    flexWrap: 'wrap'
}

Row.displayName = 'Row'

export default Row

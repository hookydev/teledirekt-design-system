import React from 'react'
import PropTypes from 'prop-types'

import Icon from './../Icon'
import Button from './../Button'

const IconButton = ({ src, ...props }) => (
    <Button {...props}>
        <Icon src={src} />
    </Button>
)

IconButton.displayName = 'IconButton'

IconButton.defaultProps = {
    fontSize: 24,
    src: PropTypes.string,
}

export default IconButton

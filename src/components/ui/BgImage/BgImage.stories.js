import React from 'react'
import { storiesOf } from '@storybook/react'

import { Box, BgImage, Heading } from './../index'

storiesOf('Components/BgImage', module).add('default', () => (
    <BgImage backgroundImage="url(http://placekitten.com/600/300)" width={600} height={300}>
        <Box alignItems="center" justifyContent="center" height="inherit" p={5}>
            <Heading fontSize={6} color="white">
                Hello
            </Heading>
        </Box>
    </BgImage>
))

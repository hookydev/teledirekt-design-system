import React from 'react'
import { ThemeConsumer } from 'styled-components'
import Box from '../Box'

const Col = props => (
    <ThemeConsumer>
        {theme => (
            <Box
                flex={props.width ? null : '1'}
                px={props.px || (theme && theme.gutters) || ['8px', '8px', '16px']}
                {...props}
            />
        )}
    </ThemeConsumer>
)

Col.displayName = 'Col'

export default Col

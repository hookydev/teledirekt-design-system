import React from 'react'
import { storiesOf } from '@storybook/react'

import { Input, Box } from './../index'

class EssayForm extends React.Component {
    state = {
        value: '',
    }

    handleChange = e => {
        this.setState({ value: e.target.value })
    }

    render() {
        return (
            <form>
                <label>
                    Essay:
                    <Input
                        placeholder="Ваш email"
                        mb={30}
                        value={this.state.value}
                        onChange={this.handleChange}
                        css={{ margin: '30px' }}
                        // placeholderType='static'
                    />
                </label>
            </form>
        )
    }
}

storiesOf('Components/Input', module).add('default', () => (
    <Box width="400px">
        <Input placeholder="Ваш e-mail" mb={2} />
        <Input placeholder="Ваш e-mail" variant="second" />
    </Box>
))

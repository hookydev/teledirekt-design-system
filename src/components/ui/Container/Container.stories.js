import React from "react"
import { storiesOf } from "@storybook/react"

import { Box, Container } from "./../index"

storiesOf("Components/Container", module)
    .add("default (responsive width from theme)", () => (
        <Container>
            <Box m={2} p={2} bg="lightblue">
                Container
            </Box>
        </Container>
    ))
    .add("width custom responsive width", () => (
        <Container maxWidth={["100px", "200px", "300px", "400px", "500px"]}>
            <Box m={2} p={2} bg="lightblue">
                Container
            </Box>
        </Container>
    ))
    .add("width fixed width", () => (
        <Container maxWidth={600}>
            <Box m={2} p={2} bg="lightblue">
                Container
            </Box>
        </Container>
    ))

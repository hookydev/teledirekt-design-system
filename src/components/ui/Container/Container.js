import React from 'react'
import styled, { ThemeConsumer } from 'styled-components'
import { style, getWidth } from 'styled-system'
import Box from '../Box'

const maxContainerWidth = style({
    prop: 'maxWidth',
    cssProperty: 'max-width',
    transformValue: getWidth
})

const BoxMax = styled(Box)(maxContainerWidth)

const Container = props => (
    <ThemeConsumer>
        {theme => (
            <BoxMax
                maxWidth={
                    props.maxWidth ||
                    (theme && theme.maxContainerWidths) || ['100%', 456, 540, 720, 960, 1140]
                }
                px={props.px || (theme && theme.gutters) || ['10px', '10px', '15px']}
                {...props}
            />
        )}
    </ThemeConsumer>
)

Container.displayName = 'Container'

Container.defaultProps = {
    mx: 'auto',
    width: '100%'
}

export default Container

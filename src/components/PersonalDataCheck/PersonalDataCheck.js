import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";

import Checkbox from "./Checkbox";


const Container = styled.div`
    font-family: Roboto, Arial, sans-serif;
    font-weight: 300;
    color: white;
    display: flex;
`;

const Text = styled.div`
    font-size: 12px;
    margin-left: 7px;
`;

const Link = styled.a`
    color: currentColor;
    text-decoration: underline;
`;

const PersonalDataCheck = ({checked}) => (
    <Container>
        <Checkbox size={18} checked={checked} />
        <Text>Нажимая кнопку «Отправить», я даю согласие на
            <Link href='https://teledirekt.ru/docs/policy-personal-data'>{' '}обработку персональных данных</Link>,
            согласие с <Link href='https://teledirekt.ru/docs/personal-data-processing'>{' '}Политикой обработки персональных данных</Link> и
            <Link href='https://teledirekt.ru/docs/custom-agreement'>{' '}Пользовательским соглашением</Link>.
            </Text>

    </Container>
);

export default PersonalDataCheck;

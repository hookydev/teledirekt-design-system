import React, { Component } from 'react'
import logo from './logo.svg'
import './App.css'

import { ThemeProvider } from 'styled-components'

import { Box, Flex, Container, Text, Link, Col } from './components/ui'
const theme = {}

const MainText = props => <Text color="red" fontWeight="bold" {...props} />
const LeftText = props => <Text color="blue" fontWeight="bold" {...props} />

class App extends Component {
    render() {
        return (
            <div className="App">
                <Box>Nigga</Box>
                <Flex>Clam</Flex>
                <ThemeProvider theme={theme}>
                    <Col bg="lightblue">Col1</Col>
                </ThemeProvider>
                <Container>
                    <Flex justifyContent="center">
                        <MainText>Привет</MainText>
                        <LeftText>Привет</LeftText>
                    </Flex>
                </Container>
                <header className="App-header">
                    <img src={logo} className="App-logo" alt="logo" />
                    <h1 className="App-title">Welcome to React</h1>
                </header>
                <p className="App-intro">
                    To get started, edit <code>src/App.js</code> and save to reload.
                </p>
            </div>
        )
    }
}

export default App
